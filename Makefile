.SUFFIXES:	.fig .gif .jpg .eps .tex .dvi .ps .pdf

CODE = MOMIB09
# CODE = pp
STYLES=JAC2003.cls
PAPERFIGS = \
    $(CODE)f1.eps \
    $(CODE)f2.eps \
    $(CODE)f3.eps \
    $(CODE)f4.eps \
    $(CODE)f5.eps
PAPER=$(CODE).pdf
ARCHIVE = Makefile $(CODE).tex
ARCHIVE += $(CODE).ps $(STYLES) $(PAPERFIGS) $(RASTERS)

# TALK=$(CODE)_talk.pdf
# ARCHIVE += $(CODE)_talk.tex

.PRECIOUS: $(CODE).ps $(PAPERFIGS)

default: $(PAPER) $(TALK) $(POSTERTEXT) view
all: clean default zip

$(CODE).dvi: $(PAPERFIGS) $(CODE).tex

.tex.dvi:
	latex $* && latex $*
.dvi.ps:
	dvips -o $@ $^
.fig.eps:
	fig2dev -L eps $^ $@
.jpg.eps:
	jpegtopnm $^ | pnmtops -noturn -nocenter > $@
.gif.eps:
	giftopnm $^ | pnmtops -noturn -nocenter > $@
.dia.eps:
	dia -t eps $^
.eps.pdf:
	epstopdf $^
.ps.pdf:
	ps2pdf $^ $@

#
# this additional step seems to be required to make
# JACoW distiller behave on courier fonts
#
submit:
	acroread -toPostScript -size a4 $(CODE).pdf
	ps2pdf $(CODE).ps

$(CODE).dvi: $(CODE).tex $(PAPERFIGS)
$(TALK): $(CODE)_talk.tex $(TALKFIGS)
	pdflatex $* && pdflatex $*

POSTERTEXT=$(CODE)_pt.pdf

clean:
	rm -rf *.aux *.dvi *.log *.bak *.pdf *.nav *.out *.snm *.toc *.ps
zip:
	zip $(CODE)_`date +%Y%m%d%H%M%S`.zip $(ARCHIVE)
view: $(PAPER)
	acroread $(PAPER) &
view_talk: $(TALK)
	acroread $(TALK) &
view_pt: $(POSTERTEXT)
	acroread $(POSTERTEXT) &
