ZIO: The Ultimate Linux I/O Framework

Juan David Gonzalez Cobas (CERN)
Alessandro Rubini, Federico Vaga, Simone Nellaga (University of Pavia, Italy)

ZIO (with Z standing for "The Ultimate I/O" Framework) was developed for
CERN with the specific needs of physics labs in mind, which are
poorly addressed in the maistream Linux kernel.

ZIO provides a framework for industrial, high-throughput, high-channel
count I/O device drivers (digitizers, function generators, timing
devices like TDCs) with performance, generality and scalability as
design goals.

Among its many features, it offers abstractions for
- input and output channels, and channel sets
- configurable trigger types
- configurable buffer types
- interface via sysfs attributes, control and data device nodes
- a socket interface (PF_ZIO) which provides enormous flexibility
  and power for remote control

In this paper, we discuss the design and implementation of ZIO,
and describe representative cases of driver development for typical and
exotic applications (FMC ADC 100Msps digitizer, FMC TDC timestamp
counter, FMC DEL fine delay).
