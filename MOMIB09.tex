\documentclass{JAC2003}

%%
%%  This file was updated in April 2009 by J. Poole to be in line with Word tempaltes
%%
%%  Use \documentclass[boxit]{JAC2003}
%%  to draw a frame with the correct margins on the output.
%%
%%  Use \documentclass[acus]{JAC2003}
%%  for US letter paper layout
%%

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{url}

%%
%%   VARIABLE HEIGHT FOR THE TITLE BOX (default 35mm)
%%

\setlength{\titleblockheight}{40mm}

\begin{document}
\title{ZIO: The Ultimate Linux I/O Framework}

\author{Alessandro Rubini (University of Pavia, Italy)\\
Juan David Gonz\'alez Cobas, Tomasz W\l ostowski (CERN)\\
Federico Vaga (GNUDD)\\
Simone Nellaga (University of Pavia, Italy)}

\maketitle

\begin{abstract}
ZIO (standing for ``The Ultimate I/O'' Framework) was developed for
CERN with the specific needs of physics labs in mind, which are
poorly addressed in the mainstream Linux kernel.

ZIO provides a framework for industrial, high-bandwidth, high-channel
count I/O device drivers (digitizers, function generators, timing
devices like TDCs) with performance, generality and scalability as
design goals.

Among its features, it offers abstractions for
\begin{Itemize}
\item both input and output channels, and channel sets
\item run-time selection of trigger types
\item run-time selection of \ buffer types
\item sysfs-based configuration
\item char devices for data and metadata
\item a socket interface (PF\_ZIO) as alternative to char devices
\end{Itemize}

In this paper, we discuss the design and implementation of ZIO,
and describe representative cases of driver development for typical and
exotic applications: drivers for the FMC (FPGA Mezzanine Card,
see~\cite{vita-fmc}) boards
developed at CERN like the FMC ADC 100Msps digitizer, FMC TDC timestamp
counter, and FMC DEL fine delay.
\end{abstract}

\section{MOTIVATION AND REQUIREMENTS}

The initial motivation behind the development of ZIO arose in 2011,
after a careful analysis of Comedi and IIO,
the I/O frameworks existing at
that time in the staging area of the Linux kernel source tree.
It was clear that both alternatives were not
suitable, generic nor complete enough for the needs of data acquisition
systems like the ones at CERN or other physics laboratory facilities,
where high performance and diversity of available hardware impose
stringent conditions. For example, block transfers, output, fine
timestamping or mmap/DMA were absent in IIO and definitely required.

As announced in~\cite{foss-2011}, the design and development of a
Linux kernel framework for I/O better suited to the
needs of physics laboratories was initiated, with these aims in mind:
\begin{Itemize}
\item digital and analog input and output;
\item one-shot and streaming (buffered) data acquisition or
        waveform play;
\item high resolution (under 1ns) timestamping of data blocks;
\item generic coverage of resolution, sampling rate,
	data sizes, calibration, offset and gain parameters;
\item pluggable buffer and trigger types;
\item low overhead;
\item support for DMA;
\item bit grouping in digital I/O;
\item clean design conforming to Linux kernel practice, with the
intention to integrate in the mainstream kernel.
\end{Itemize}

\subsection{Development and Release Timeline}

Alessandro Rubini and Federico Vaga started ZIO development in
October~2011, as part of their collaboration with CERN BE/CO/HT
section~\cite{federico-laurea}, within the Open Hardware initiative.
Its ongoing development can be followed in the project page at
the Open Hardware Repository,
\url{http://www.ohwr.org/projects/zio}.

By May 2012, ZIO was mature enough to make its real-world debut in the
experimental setting of the LNGS neutrino speed
measurements~\cite{lngs}. At around the same time, development of the
PF\_ZIO network type was started by Simone Nellaga~\cite{simone-laurea}.

The first official release of ZIO v1.0 appeared in January 2013.
Contributions have continued ever since by the three core developers,
adding features, test benches, fixing bugs and the ongoing development
of PF\_ZIO.

\section{HOW ZIO WORKS}

As a Linux kernel I/O framework, ZIO has to provide the
following basic abstractions:
\begin{Itemize}
\item
a view of I/O devices and their features,
\item
a mechanism to pipe data from user space
applications to the raw hardware and vice versa,
\item
a model of the data it handles, with associated metadata,
\item
an appropriate interface to user space to access it all 
in a uniform way
\end{Itemize}
The following sections explain how ZIO addresses the above
abstractions in its own peculiar ways.

\section{HOW ZIO SEES I/O DEVICES}

\subsection{Devices, Buffers and Triggers}

I/O peripheral devices transfer data (dealt with by ZIO in so-called
data blocks, to be described later) through channels.
Input or output actions can happen in response to events
related to external signals, timing, or even self-timing of the device;
in addition, some kind of buffer has to be in place to host
the transferred data. According to this view, ZIO deals with
abstractions for
\begin{Description}
\item[devices] which correspond to a specific I/O peripheral
\item[triggers] software objects which provoke I/O events when 
    a condition they are prepared (or \emph{armed}) to respond to occurs
\item[buffers] which take care of holding data passed back and forth
\end{Description}

\begin{figure}[htb]
   \centering
   \includegraphics*[width=\columnwidth]{MOMIB09f1.eps}
   \caption{The ZIO framework hierarchy}
   \label{fig-hierarchy}
\end{figure}

The standard ZIO distribution provides ready-to-use
examples of these three concepts.
In particular, generic RAM-based buffer types are provided based on
\texttt{kmalloc} and \texttt{vmalloc}. Basic trigger types
that come with stock ZIO are:
\begin{Description}
\item[transparent trigger] activated by software read/write request or
by self-timing devices
\item[kernel timer] activated periodically
\item[high resolution timer] periodic or one-shot
\item[external] activated by external interrupt
\end{Description}
Additionally, add-on drivers can register their own trigger type,
so for example an ADC card can provide data-driven triggers for
a scope-like application. An example is the FMC ADC driver developed
by Federico Vaga which we will reference later.

\subsection{Channel Sets}

The core concept of the device model of ZIO is the \emph{channel set}
(cset for short): a set of channels of identical characteristics that is
associated with a trigger instance. Such an instance is a software
object prepared to react to a particular type of trigger event and
provoke the actual I/O action. Trigger instances are, as one could
expect, instantiations of trigger types. The consequence is that all
channels in a cset are always affected as a whole
by a particular I/O event. A device can contain various csets, which
gives rise to the hierarchical structure of Fig.~\ref{fig-hierarchy}.

In addition, each channel in a cset gets a buffer instance
of a buffer type
associated with the cset.

To summarize, csets contain channels whose I/O events are driven by
a trigger instance of a particular type, i.e., reacting to a particular
class of trigger events; the cset channels are
homogeneous and have buffers, all belonging to the cset buffer type.
The relationships are displayed in Fig.~\ref{fig-cset}.

\begin{figure}[htb]
   \centering
   \includegraphics*[width=65mm]{MOMIB09f2.eps}
   \caption{A ZIO channel set (cset)}
   \label{fig-cset}
\end{figure}

\section{HOW ZIO SEES DATA FLOW: THE DATA PIPELINE}

In ZIO, each I/O event is the transfer of a data block, which consists
of zero or more samples.  The path a data block traverses between the I/O
peripheral and the user space application is depicted in
Fig.~\ref{fig-pipeline}.

The lifetime of a data block during a transfer is easy to understand
from the picture. In a \verb|read| call, the buffer receives a
\verb|retr_block| request that it honours if an input block is
already buffered. Otherwise, the buffer requests the trigger
with a \verb|pull_block|
to notify the trigger that a transfer should proceed; most often,
this method is absent and the trigger acts by itself.
The actual I/O is initiated when the trigger calls \verb|raw_io|, its
completion being notified to the trigger by a \verb|data_done| callback.

Output occurs in a dual manner, the roles of all methods being identical
or parallel to the input case.

For the many particular corner cases that may
arise we refer to the user manual~\cite{manual}.

\begin{figure*}[tb]
   \centering
   \includegraphics*[width=130mm]{MOMIB09f3.eps}
   \caption{The ZIO data pipeline}
   \label{fig-pipeline}
\end{figure*}

It is interesting to note the following
\begin{Itemize}
\item the input and output pipelines are symmetric.
\item ZIO components have a simple core interface. Buffers provide
methods for the allocation, storage, retrieval and freeing of the
active data block. Triggers, on their side, communicate with the
peripheral driver with \verb|raw_io|  and \verb|data_done| methods;
in addition, explicit read/write requests are communicated to triggers via
\verb|pull_block| and \verb|push_block| methods.
\item the overall flow is very simple: in normal conditions, all
that is involved is \verb|store_block| and \verb|retr_block|
for the buffers, and \verb|raw_io| and \verb|data_done| for the
triggers. In addition \verb|push_block| and \verb|pull_block|
can explicitly initiate transfers, or restart them when they stopped
because of data starvation.
\end{Itemize}

\section{HOW ZIO SEES DATA: THE DATA MODEL}

The most original and fertile idea in ZIO is its data model, based on a
structure named~\emph{block}. A block consists of two parts
\begin{Itemize}
\item metadata associated with the I/O event that produced the data,
    in the form of a fixed-size structure called a \emph{control}
    in ZIO jargon.
\item the actual data, a possibly huge number of samples, in the form
    of a payload completely transparent to ZIO.
\end{Itemize}

It is interesting to take a look at the layout of the control data
structure, as depicted in Fig.~\ref{fig-ctrl}. 

The structure is defined to be always 512 bytes long.  The two most
important sub-structures in the control are a high-resolution time
stamp (green in the figure), which can be either software- or
hardware-generated, and a complete, world-unique, identification of
the channel this block belongs to (gray in the figure).  Such data
structure is used as socket address in PF\_ZIO, so applications
can deterministically \verb|sendto()| and \verb|recvfrom()| in the ZIO network.

The leading fields identify the version of the structure, the sequence
number and size of this block, as well as alarm bits to persistently
report errors in the stream for this channel (alarm conditions are
reset by bitwise sysfs writes).

The bulk of the control structure then lists attributes for both the
current channel and the current trigger.  In this way, a ZIO block
carries the complete metadata information together with the data,
offering a flexible transport interface where only the endpoints
of the ZIO pipeline are concerned with the inner details of the
specific device or trigger.

\begin{figure}[htb]
   \centering
   \includegraphics*[width=\columnwidth]{MOMIB09f4.eps}
   \caption{The ZIO control structure}
   \label{fig-ctrl}
\end{figure}


\section{HOW ZIO IS SEEN FROM USER SPACE}

ZIO interfaces with user space by means of character devices,
two per channel: one for control and one for data. The most common data
flow is depicted in Fig.~\ref{fig-cdev}. In any case, the user can
choose not to read or write either the control or the data; the
semantics allows an application, for example, to ignore the metadata
if it knows it is acquiring a stream of similar data blocks.
More complex and demanding data flows are possible, allowing access to
mmapped data using the vmalloc buffer type.

\begin{figure}[htb]
   \centering
   \includegraphics*[width=\columnwidth]{MOMIB09f5.eps}
   \caption{The ZIO control and data streams}
   \label{fig-cdev}
\end{figure}

ZIO also offers a rich sysfs interface, with sets of standard and
extended attributes assigned to devices, triggers and csets. This
results from the basic ZIO design principle of ``no \texttt{ioctl()},
thanks'' for out-of-band device configuration. Apart from
the intrinsic interest as an interface, sysfs attributes prove
invaluable in testing and debugging. The attributes are automatically
mapped to the control structure, so the sysfs and the char device views
are consistent.

Last, but not least, PF\_ZIO is another development in beta stage that
allows to perform I/O through a standard socket interface.
Using a single PF\_ZIO
socket, an application can exchange data blocks with several channels;
this is especially useful when collecting events from sensor networks
with sporadic data but high cardinality---like neutrino detectors.
PF\_ZIO supports all three types of socket: stream, datagram and raw.


\section{ZIO IN PRACTICE: FMC ZIO DEVICE DRIVERS}

The BE-CO-HT section at CERN has developed a kit of modules described
in~\cite{fpga-fmc,final-fmc}, compliant with the ANSI VITA~57 FMC (FPGA
Mezzanine Card) standard~\cite{vita-fmc}. 
Linux device drivers for the following
modules have been developed using the ZIO framework.
\begin{Itemize}
\item  FMC Delay 1ns 4cha
	(\url{http://www.ohwr.org/projects/fmc-delay-1ns-8cha})
\item FMC ADC 100M 14b 4cha
	(\url{http://www.ohwr.org/projects/fmc-adc-100m14b4cha})
\item FMC TDC 1ns 5ch
	(\url{http://www.ohwr.org/projects/fmc-tdc})
\end{Itemize}

Although both the ADC and the TDC are good examples of devices ZIO
was conceived for, it is by an accident of history that the FMC Fine
Delay was the first CERN device whose low-level software was entirely
ZIO-based.

The first stable release of the ZIO-based FMC ADC driver appeared in
July 2013. Interestingly, this module's particular features (e.g.
multi-shot acquisition) required the development of a specific trigger
type. Again, the flexibility built into the framework made it adapt
smoothly to new requirements.

The FMC TDC drivers are in the final stage of development and
will be probably released by the time this paper is
published. In this
particular case, none of the two developers were
members of the original ZIO core development team. They used ZIO
to great profit without much difficulty in learning its internals.

\section{CONCLUSIONS}

The experience of the FMC drivers development shows that a diversity
of devices is handled well within the ZIO framework, even when
new features require special types of trigger or buffer.

It might appear that the various abstractions built into ZIO make its
learning curve steep; however, as the FMC TDC example shows, developers
can quickly get up to speed with it and produce drivers for complex
hardware. This is made possible by reference implementations (the FMC
Fine Delay driver) and instructive example drivers supplied by the ZIO
standard distribution.
Moreover, a common interface and set of tools for monitoring
and debugging the developed drivers reward the initial learning effort.

\begin{thebibliography}{9}   % Use for  1-9  references
%\begin{thebibliography}{99} % Use for 10-99 references

\bibitem{vita-fmc}
VME International Trade Association,
``FPGA Mezzanine Card (FMC) Standard'', \url{http://www.vita.com/}

\bibitem{foss-2011}
J.D. Gonzalez Cobas, S. Iglesias Gonsalvez, J.H. Lewis, J. Serrano, M.
Vanga, E.G. Cota, A. Rubini and F. Vaga, ``Free and Open Source Software
at CERN: Integration of Drivers in the Linux Kernel'', ICALEPCS'2011,
Grenoble, October 2011, THCHMUST04, pp.1248-1251 (2011),
\texttt{http://www.JACoW.org}.

\bibitem{federico-laurea}
F. Vaga, ``Development of an I/O framework
within the Linux kernel for
high-bandwidth data transfers'', Tesi di Laurea,
Facolt\`a Di Ingegneria,
Universit\`a degli
Studi di Pavia, 2011.

\bibitem{lngs}
F. Pietropaolo \emph{et al},
``Precision measurement of the neutrino velocity with the ICARUS
detector in the CNGS beam'', Journal of High Energy Physics,
2012~(11):~49, pp.~1--21.

\bibitem{simone-laurea}
S. Nellaga, ``Realizzazione di un protocollo di rete per
Input/Output industriale'', Tesi di Laurea,
Facolt\`a Di Ingegneria,
Universit\`a degli
Studi di Pavia, 2012.

\bibitem{manual}
A. Rubini and F. Vaga, ``ZIO User Manual (version 1.0)'',
\url{http://www.ohwr.org/attachments/1896/zio-manual-130121-v1.0.pdf}.

\bibitem{fpga-fmc}
P. Alvarez, M. Cattin, J. H. Lewis, J. Serrano and T. Wlostowski,
``FPGA Mezzanine Cards for CERN’s Accelerator Control System'',
in ICALEPCS'09, p. 376, 2009.

\bibitem{final-fmc}
E. Van der Bij, M. Cattin, E.  Gousiou, J. Serrano and T. Wlostowski,
`` CERN's FMC Kit'', ICALEPCS'2013 (to appear).

\end{thebibliography}
\end{document}
